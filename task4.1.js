class Elements {
  constructor(className, html) {
    this.html = html;
    this.className = className;
  }

  render() {
    const element = document.querySelector(this.className);
    if(element){
      element.innerHTML = this.html
    }
  }

  delete() {
    const element = document.querySelector(this.className);
    element.remove();
  }
}
  
class Images extends Elements {
  constructor(className, src) {
    super(className);
    this.src = src;
  }

  imgRender() {
    const parentBlock = document.querySelector(this.className);
    const image = document.createElement("img");
    image.src = this.src;
    parentBlock.append(image);
  }
}


const newH1 = new Elements('.h1', '<h1>Smallface Man</h1>');
newH1.render();

const newH3 = new Elements('.h3__1', '<h3>JavaScript Developer</h3>');
newH3.render();

const newPContacts = new Elements('.p__contacts', '<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et, rerum hic quaerat sequi officiis eum numquam veniam deleniti, explicabo magnam animi perferendis tenetur similique? Quisquam, beatae. Blanditiis ea enim sunt.</p>');
newPContacts.render();

const newPRecommends = new Elements('.p__recommends', '<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et, rerum hic quaerat sequi officiis eum numquam veniam deleniti, explicabo magnam animi perferendis tenetur similique? Quisquam, beatae. Blanditiis ea enim sunt.</p>');
newPRecommends.render();

const newPUserData = new Elements('.p__user__data', '<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Et, rerum hic quaerat sequi officiis eum numquam veniam deleniti, explicabo magnam animi perferendis tenetur similique? Quisquam, beatae. Blanditiis ea enim sunt.</p>');
newPUserData.render();

const newUlUserData = new Elements('.user__ul', '<ul><li>node</li><li>react</li><li>ES5</li><li>ES6</li><li>webpack</li><li>sass</li><li>mongoDB</li><li>babel</li></ul>');
newUlUserData.render();
  
const newImg = new Images('.user__photo', 'https://img-9gag-fun.9cache.com/photo/3532405_460s.jpg');
newImg.imgRender();

const newH5Work = new Elements('.workname', '<h5>Team Lead JS</h5>');
newH5Work.render();

const newH5Workceo = new Elements('.workceo', '<p>Company: TopSkill LD</p>');
newH5Workceo.render();

const newUlWorkData = new Elements('.work__attributes', '<ul><li>work</li><li>work</li><li>work</li><li>work</li><li>work</li></ul>');
newUlWorkData.render();

const newEduName = new Elements('.edu__name', '<h5>BGU</h5>');
newEduName.render();

const newEduAttributes = new Elements('.edu__attributes', '<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quo eligendi cupiditate animi ipsam nesciunt hic ex ut consequuntur illo quia dolorem, tempora, voluptate veritatis non debitis enim magnam perspiciatis. Quaerat?</p>');
newEduAttributes.render();

const newFooter = new Elements('.footer', '<p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quo eligendi cupiditate animi ipsam nesciunt hic ex ut consequuntur illo quia dolorem, tempora, voluptate veritatis non debitis enim magnam perspiciatis. Quaerat?</p>');
newFooter.render();

class AddData extends Elements{
  constructor(className, html){
    super(className, html)
  }
  static add(className, html){
    const addData = document.querySelector('#addData');
    addData.addEventListener('click', function() {
      const parentBlock = document.querySelector(className);
      parentBlock.insertAdjacentHTML('beforeend', html);
      changed.changes();
      saved.saves();
    });
  }
}
AddData.add('.p__user__data',`<p contenteditable="true" class="borders">some new data</p>`);


class Save{
  constructor(save) {
      this.save = save;
      }

  saves() { 
      let p = document.querySelectorAll('p');
      let h5 = document.querySelectorAll('h5');
      let h4 = document.querySelectorAll('h4');
      let h3 = document.querySelectorAll('h3');
      let h1 = document.querySelector('h1');
      let ul = document.querySelectorAll('ul');
      const change = document.querySelector('#change');
      const save = document.querySelector('#save');    
      const addData = document.querySelector('#addData');    
      this.save.addEventListener('click', function() {
      h1.setAttribute('contenteditable', false);
      h1.classList.remove('borders');
      p.forEach(el => {
          el.setAttribute('contenteditable', false);
          el.classList.remove('borders');
      });
      h5.forEach(el => {
          el.setAttribute('contenteditable', false);
          el.classList.remove('borders');
      });
      h3.forEach(el => {
          el.setAttribute('contenteditable', false);
          el.classList.remove('borders');
      });
      h4.forEach(el => {
          el.setAttribute('contenteditable', false);
          el.classList.remove('borders');
      });
      ul.forEach(el => {
          el.setAttribute('contenteditable', false);
          el.classList.remove('borders');
      });
      change.classList.remove('none');
      save.classList.add('none');
      addData.classList.add('none');
      
  });}
}
let saved = new Save(save);
saved.saves();

class Change extends AddData{
  constructor(change, className, html) {
    super(className, html)
      this.change = change;
  }
  changes()
      {
          let p = document.querySelectorAll('p');
          let h5 = document.querySelectorAll('h5');
          let h4 = document.querySelectorAll('h4');
          let h3 = document.querySelectorAll('h3');
          let h1 = document.querySelector('h1');
          let ul = document.querySelectorAll('ul');
          const change = document.querySelector('#change');
          const save = document.querySelector('#save');
          const addData = document.querySelector('#addData');
          this.change.addEventListener('click', function() {
          h1.setAttribute('contenteditable', true);
          h1.classList.add('borders');
          p.forEach(el => {
              el.setAttribute('contenteditable', true);
              el.classList.add('borders');
          });
          h5.forEach(el => {
              el.setAttribute('contenteditable', true);
              el.classList.add('borders');
          });
          h3.forEach(el => {
              el.setAttribute('contenteditable', true);
              el.classList.add('borders');
          });
          h4.forEach(el => {
              el.setAttribute('contenteditable', true);
              el.classList.add('borders');
          });
          ul.forEach(el => {
              el.setAttribute('contenteditable', true);
              el.classList.add('borders');
          });
          change.classList.add('none');
          save.classList.add('save');
          addData.classList.add('save');
          save.classList.remove('none');
          addData.classList.remove('none');
      });
  }
}
let changed = new Change(change);
changed.changes();

class Theme {
  toggle(){
    let toggle = document.querySelector('.switch');
    let footer = document.querySelector('footer');
    let header = document.querySelector('.switch__block');
    let bodyBlock = document.querySelector('#data__block');
    let leftBlock = document.querySelector('.left__block');
    toggle.addEventListener('change', function(){
      header.classList.toggle("toggle__color2");
      leftBlock.classList.toggle("toggle__color1");
      bodyBlock.classList.toggle("toggle__color2");
      footer.classList.toggle("toggle__color1");
   }); 
  }
}
const toggled = new Theme();
toggled.toggle();
